/*
 * grunt-json-to-ang
 * 
 *
 * Copyright (c) 2015 Jordan D Chapian
 * Licensed under the MIT license.
 */

'use strict';

function stringify(obj) {
  var placeholder = '____PLACEHOLDER____';
  var fns = [];
  var json = JSON.stringify(obj, function(key, value) {
    if (typeof value === 'function') {
      fns.push(value);
      return placeholder;
    }
    return value;
  }, 2);
  json = json.replace(new RegExp('"' + placeholder + '"', 'g'), function(_) {
    return fns.shift();
  });
  return json;
};

module.exports = function(grunt) {

  grunt.registerMultiTask('json_to_ang', 'Converts JSON or .js constant files to angular constants, so that the same constant files can be used on the client and server seamlessly.', function() {

    var tasks = grunt.config('json_to_ang');
    var config = grunt.config('json_to_ang').__;

    var buffer = (config.createModule) ? 'angular.module("'+config.baseModule+'", [])\n' : 'angular.module("'+config.baseModule+'")\n';

    for(var taskName in tasks){
      if(taskName === '__')continue;

      var taskDefinition = {
        name: taskName,
        location: tasks[taskName]
      };

      if(taskDefinition.location === undefined){
        grunt.fail.warn("Must provide input location for task: "+ taskName);
        return;
      }

      var fileType = taskDefinition.location.substr(taskDefinition.location.lastIndexOf('.') + 1);

      var constantBody;
      if(fileType === 'js'){
         constantBody = require(taskDefinition.location);
      }

      else if(fileType === 'json'){
        constantBody = grunt.file.readJSON(taskDefinition.location);
      }
      
      buffer += '.constant("'+taskDefinition.name+'",\n'+stringify(constantBody,null, 1)+')\n';
    }

    buffer+=';';
    grunt.file.write(config.output, buffer);
    grunt.log.writeln("File "+ config.output + " created.");
  });
};
